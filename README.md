## AWS LAMBDA FUNCTION 

***

I used the AWS Lambda service to implement a lambda function in python that reads the S3 bucket. 
If the object that's been added to the bucket is a image, the lambda takes the image and uses AWS Rekognition
to get the labels of the image. 

***
#### SeeFood

The function checks the labels, output of AWS Rekognition, of the image whether it contains Hot Dog or not. 
It then logs a key value pair with file name as the key and "Hot Dog" or "Not Hot Dog" as the value.

### The Code

The html file in the repo, contains the step by step process of implementing this.

The folder 'code' in the repo contains the python code (also in html format).
